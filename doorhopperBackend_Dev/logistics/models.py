from django.db import models

# Create your models here.
class Shipments(models.Model):
	shipment = models.AutoField(primary_key=True,db_index=True)
	address = models.ForeignKey()
	tracking_id =  models.CharField(max_length=20)
	delivery_boy = models.ForeignKey()
	status = models.CharField(max_length=2)
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	updated_date = models.DateField(auto_now_add=True)
	updated_time = models.TimeField(auto_now_add=True)
	payment_status = models.CharField(max_length=2)
	payment_mode = models.CharField(max_length=4)
	item_no = models.PositiveSmallIntegerField()
	customer_message = models.TextField()

































































