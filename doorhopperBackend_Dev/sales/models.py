from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from stores.models import ProductStoreContent,ProductStoreContentVariant,ServiceStoreContent,ServiceStoreContentVariant
from products.models import Products
from services.models import  Services
# For Products

# Cart
class PCart(models.Model):
	cart = models.AutoField(primary_key=True,db_index=True)
	customer = models.ForeignKey(User)
	cart_status = models.CharField(max_length=2)
	date = models.DateField(auto_now=True)
	time = models.TimeField(auto_now=True)
	identifier = models.CharField(max_length=64,default='SOME_UNIQUE_ID')

# All cart content
# Known Item from Known Shop
class RItemRShop(models.Model):
	cart = models.ForeignKey(Pcart,db_index=True)
	product = models.ForeignKey(ProductStoreContent)
	variant = models.ForeignKey(ProductStoreContentVariant)
	quantity = models.PositiveIntegerField()

# Known Item form unknown shop
class RItemUShop(models.Model):
	cart = models.ForeignKey(Pcart,db_index=True)
	product = models.ForeignKey(Products)
	variant = models.CharField(max_length=20)
	quantity = models.PositiveIntegerField()
	shop_name = models.CharField(max_length=50)
	shop_address = models.ForeignKey()
	comment = models.TextField()

# Unknown Item from unknown shop
class UItemUShop(models.Model):
	cart = models.ForeignKey(Pcart,db_index=True)
	product_name = models.CharField(max_length=20)
	product_variant = models.CharField(max_length=10)
	barcode = models.CharField(max_length=128)
	image = models.FileField(upload_to=None, max_length=100, attributes)
	shop_name = models.CharField(max_length=50)
	shop_address = models.ForeignKey()
	quantity = models.PositiveIntegerField()
	comment = models.TextField()

# Cumulative order
class PCorder(models.Model):
	corder = models.AutoField(primary_key=True,db_index=True)
	customer = models.ForeignKey(User)
	cart = models.ForeignKey(Pcart)
	pickup_address = models.ForeignKey()
	delivery_address = models.ForeignKey()
	status = models.CharField(max_length=2,default='AA')
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	identifier = models.CharField(max_length=64,default='SOME_UNIQUE_ID')
	products_bill = models.DecimalField(max_digits=8, decimal_places=2)
	shipment = models.ForeignKey()
	invoice = models.OneToOneField()
	transaction = models.OneToOneField()
	payment = models.OneToOneField()
	revenuegenerated = models.OneToOneField()
	Servicetax_id = models.ForeignKey()
	Service_taxcollection = models.OneToOneField()
	delilvery_charges = models.DecimalField(max_digits=6, decimal_places=2)
	total_bill = models.DecimalField(max_digits=7, decimal_places=2)

# Corder content - Orders with known products from knows Shop
class KPOrder(models.Model):
	order = models.AutoField(primary_key=True,db_index=True)
	corder = models.ForeignKey(PCorder,db_index=True)
	product = models.ForeignKey(ProductStoreContent)  # Products from store
	variant = models.ForeignKey(ProductStoreContentVariant)
	quantity = models.PositiveIntegerField()
	price = models.DecimalField(max_digits=7, decimal_places=2)
	seller = models.ForeignKey('serviceproviders.SellerDetails')
	coupon_applied = models.BooleanField(default=False)
	coupon = models.ForeignKey('products.Coupon',null=True)
	discount_type = models.CharField(max_length=2,null=True)
	discount_figure = models.DecimalField(max_digits=6,decimal_places=2,default=0000.00)
	discount_amount = models.DecimalField(max_digits=6,decimal_places=2,default=0000.00)
	revenuegenerated = models.DecimalField(max_digits=6,decimal_places=2,default=0000.00)
	tax = models.ForeignKey('Tax',null=True)
	tax_percent = models.DecimalField(max_digits=2, decimal_places=2,default=00.00)
	tax_amount = models.DecimalField(max_digits=7, decimal_places=2,default=00000.00)
	status = models.CharField(max_length=2,default='AA')
	total_amount = models.DecimalField(max_digits=7,decimal_places=2)

# Corder content - Orders with unknown Product from unknown Shop and known product from unknown shop
class UkPOrder(models.Model):
	order = models.AutoField(primary_key=True,db_index=True)
	corder = models.ForeignKey(PCorder,db_index=True)
	product = models.ForeignKey(Products)  # Standard products
	barcode = models.CharField(max_length=128)
	image = models.FileField(upload_to=None, max_length=100, attributes)
	shop_name = models.CharField(max_length=50)
	shop_address = models.ForeignKey()
	product_name = models.CharField(max_length=20)
	product_variant = models.CharField(max_length=10)
	quantity = models.PositiveIntegerField()
	comment = models.TextField()
	price = models.DecimalField(max_digits=7, decimal_places=2)
	tax = models.ForeignKey()
	tax_percent = models.DecimalField(max_digits=2, decimal_places=2,default=00.00)
	tax_amount = models.DecimalField(max_digits=7, decimal_places=2,default=00000.00)
	status = models.CharField(max_length=2,default='AA')
	total_amount = models.DecimalField(max_digits=7,decimal_places=2)

# For Services

# Cart
class SCart(models.Model):
	cart = models.AutoField(primary_key=True,db_index=True)
	customer = models.ForeignKey(User)
	cart_status = models.CharField(max_length=2)
	date = models.DateField(auto_now=True)
	time = models.TimeField(auto_now=True)
	identifier = models.CharField(max_length=64,default='SOME_UNIQUE_ID')

# All Scart content
# Known Service from Known Service provider
class RServiceRProvider(models.Model):
	cart = models.ForeignKey(SCart,db_index=True)
	service = models.ForeignKey()
	variant = models.ForeignKey()
	duration = models.DecimalField(max_digits=None, decimal_places=None, attributes)
	
# Known Item form unknown shop
class RServiceUProvider(models.Model):
	cart = models.ForeignKey(SCart,db_index=True)
	service = models.ForeignKey()
	variant = models.CharField(max_length=20)
	duration = models.DecimalField(max_digits=None, decimal_places=None, attributes)
	provider_name = models.CharField(max_length=50)
	provider_address = models.ForeignKey()
	comment = models.TextField()

# Unknown Item from unknown shop
class UServiceUProvider(models.Model):
	cart = models.ForeignKey(SCart,db_index=True)
	image = models.FileField(upload_to=None, max_length=100, attributes)
	provider_name = models.CharField(max_length=50)
	provider_address = models.ForeignKey()
	service_name = models.CharField(max_length=20)
	service_variant = models.CharField(max_length=10)
	duration = models.DecimalField(max_digits=None, decimal_places=None, attributes)
	comment = models.TextField()

# Cumulative order
class SCorder(models.Model):
	corder = models.AutoField(primary_key=True,db_index=True)
	customer = models.ForeignKey(User)
	cart = models.ForeignKey(SCart)
	pickup_address = models.ForeignKey()
	delivery_address = models.ForeignKey()
	status = models.CharField(max_length=2,default='AA')
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	identifier = models.CharField(max_length=64,default='SOME_UNIQUE_ID')
	services_bill = models.DecimalField(max_digits=8, decimal_places=2)
	shipment = models.ForeignKey()    #  Or something Else
	invoice = models.OneToOneField()
	transaction = models.OneToOneField()
	payment = models.OneToOneField()
	revenuegenerated = models.OneToOneField()
	Servicetax_id = models.ForeignKey()
	Service_taxcollection = models.OneToOneField()
	delilvery_charges = models.DecimalField(max_digits=6, decimal_places=2)  #  or Something Else
	total_bill = models.DecimalField(max_digits=7, decimal_places=2)

# Corder content - Orders with known services from knows Shop
class KSOrder(models.Model):
	order = models.AutoField(primary_key=True,db_index=True)
	corder = models.ForeignKey(SCorder,db_index=True)
	service = models.ForeignKey()
	variant = models.ForeignKey()
	duration = models.PositiveIntegerField()
	rate = models.PositiveSmallIntegerField()
	provider = models.ForeignKey()
	coupon_applied = models.BooleanField(default=False)
	coupon = models.ForeignKey(,null=True)
	revenuegenerated = models.DecimalField(max_digits=6,decimal_places=2,default=0000.00)
	discount_type = models.CharField(max_length=2,null=True)
	discount_figure = models.DecimalField(max_digits=6,decimal_places=2,default=0000.00)
	discount_amount = models.DecimalField(max_digits=6, decimal_places=2,default=0000.00)
	tax = models.ForeignKey(,null=True)
	tax_percent = models.DecimalField(max_digits=2, decimal_places=2,default=00.00)
	tax_amount = models.DecimalField(max_digits=7, decimal_places=2,default=00000.00)
	status = models.CharField(max_length=2,default='AA')
	total_amount = models.DecimalField(max_digits=7,decimal_places=2)

# Corder content - Orders with unknown services from unknown Shop and known services from unknown shop
class UkSOrder(models.Model):
	order = models.AutoField(primary_key=True,db_index=True)
	corder = models.ForeignKey(SCorder,db_index=True)
	service = models.ForeignKey()
	duration = models.PositiveIntegerField()
	rate = models.PositiveSmallIntegerField()
	provider = models.ForeignKey()
	variant = models.ForeignKey()
	image = models.FileField(upload_to=None, max_length=100, attributes)
	provider_name = models.CharField(max_length=50)
	provider_address = models.ForeignKey()
	service_name = models.CharField(max_length=20)
	service_variant = models.CharField(max_length=10)
	comment = models.TextField()
	rate = models.PositiveSmallIntegerField()
	tax = models.ForeignKey(,null=True)
	tax_percent = models.DecimalField(max_digits=2, decimal_places=2,default=00.00)
	tax_amount = models.DecimalField(max_digits=7, decimal_places=2,default=00000.00)
	status = models.CharField(max_length=2,default='AA')
	total_amount = models.DecimalField(max_digits=7,decimal_places=2)

# Invoice
class Invoice(models.Model):
	invoice = models.AutoField(primary_key=True,db_index=True)
	delivery_address = models.ForeignKey('users.Address')
	customer = models.ForeignKey(User)
	customer_name = models.CharField(max_length=20)
	payment = models.ForeignKey(Payment,null=True)
	productORservice_bill = models.DecimalField(max_digits=7, decimal_places=2)
	shipping_charges = models.DecimalField(max_digits=4,decimal_places=2,default=00.00)
	total_bill = models.DecimalField(max_digits=7, decimal_places=2)
	generated_date = models.DateField(auto_now_add=True)
	generated_time = models.TimeField(auto_now_add=True)

# Transaction Details
class Transaction(models.Model):
	transaction = models.AutoField(primary_key=True,db_index=True)
	customer = models.ForeignKey(User)
	transaction_mode = models.CharField(max_length=10)
	transaction_amount = models.DecimalField(max_digits=7, decimal_places=2)
	transaction_signature = models.CharField(max_length=64,default='GENERATED_SIGNATURE')
	generated_date = models.DateField(auto_now_add=True)
	generated_time = models.TimeField(auto_now_add=True)
	status = models.CharField(max_length=2)
	identifier = models.CharField(max_length=64,default='SOME_UNIQUE_ID')

# Payment Details
class Payment(models.Model):
	payment = models.AutoField(primary_key=True,db_index=True)
	customer = models.ForeignKey(User)
	generated_date = models.DateField(auto_now_add=True)
	generated_time = models.TimeField(auto_now_add=True)
	transaction = models.ForeignKey(Transaction)
	amount = models.DecimalField(max_digits=7, decimal_places=2)
	payment_mode = models.CharField(max_length=4)
	done_by = models.CharField(max_length=20,default='CUSTOMER')
	recieved_by = models.CharField(max_length=20,default='DELIVERY_BOY')

# Calculated revenue
class RevenueGenerated(models.Model):
 	revenuegenerated = models.AutoField(primary_key=True,db_index=True)
 	revenue_amount = models.DecimalField(max_digits=6, decimal_places=2)
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	seller = models.ForeignKey()
	customer = models.ForeignKey(User)

#Tax COllected
class TaxCollection(models.Model):
	taxcollection = models.AutoField(primary_key=True,db_index=True)
	tax_amount = models.DecimalField(max_digits=6, decimal_places=2)
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	seller = models.ForeignKey('serviceproviders.SellerDetails')
	customer = models.ForeignKey(User)

