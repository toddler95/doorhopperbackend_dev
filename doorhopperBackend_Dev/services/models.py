from django.db import models

# Create your models here.
SERVICE_CATEGORY = (
	('AY','Anything'),
	('FD','Food'),
	('ST','Stationary'),
	('GY','Grocery'),
	('CS','Cosmetics'),
	('BK','Bakery'),
	('SW','Swadeshi'),
	('SW','Sweets'),
	('HW','Hardware'),
	('MS','Music'),
	('SP','Sports'),
	('EL','Electronics'),
	('ME','Medical'),
	)


# Service Details
class Services(models.Model):
	service = models.AutoField(primary_key=True,db_index=True)
	service_name = models.CharField(max_length=50)
	brand = models.CharField(max_length=20)
	serviceCategory = models.CharField(max_length=,choices=SERVICE_CATEGORY)

# Service Attributes
class ServiceAttributes(models.Model):
	attribute = models.AutoField(primary_key=True)
	ofService = models.ForeignKey(Services,db_index=True)
	attribute_name = models.CharField(max_length=20)
	attribute_value = models.CharField(max_length=20)

# Service Category - Used for Catalogueing
class ServiceCategory(models.Model):
	category_id = models.AutoField(primary_key=True)
	category_name = models.CharField(max_length=50)
	parent_category = models.ForeignKey(ServiceCategory)
	category_level = models.PositiveSmallIntegerField()
	has_child = models.BinaryField(default=False)

# Tags
class STags(models.Model):
	stag_id = models.AutoField(primary_key=True)
	stagWord = models.CharField(max_length=20)

# Tag relations
class STagRelations(models.Model):
	stag_id = models.ForeignKey(STags)
	service_id = models.ForeignKey(Services)

	class Meta:
		index_together = [ "stag_id","service_id" ]