from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from stores.models import ProductStoreContentVariant,ServiceStoreContentVariant
# Merchants
class Merchants(models.Model):
	merchant = models.AutoField(primary_key=True,db_index=True)
	user = models.ForeignKey(User,unique=True)


# Cumulative Order
class MPCorder(models.Model):
	corder = models.AutoField(primary_key=True,db_index=True)
	merchant = models.ForeignKey(Merchants)
	status = models.CharField(max_length=2)
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	total_bill = models.DecimalField(max_digits=7, decimal_places=2, default=00000.00)

# Cumuative Order Content
class MPCorderContent(models.Model):
	order = models.AutoField(primary_key=True,db_index=True)
	corder = models.ForeignKey(MCorder)
	product = models.ForeignKey(ProductStoreContentVariant)
	quantity = models.PositiveIntegerField()
	bill = models.DecimalField(max_digits=7, decimal_places=2, default=00000.00)

# Service
#Cumulative Order
class MSCorder(models.Model):
	corder = models.AutoField(primary_key=True,db_index=True)
	merchant = models.ForeignKey(Merchants)
	status = models.CharField(max_length=2)
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	total_bill = models.DecimalField(max_digits=7, decimal_places=2, default=00000.00)

#Corder Content
class MSCorderContent(models.Model):
	order = models.AutoField(primary_key=True,db_index=True)
	corder = models.ForeignKey(MSCorder)
	service = models.ForeignKey(ServiceStoreContentVariant)
	duration = models.DecimalField(max_digits=4, decimal_places=2)
	bill = models.DecimalField(max_digits=7, decimal_places=2, default=00000.00)










































































