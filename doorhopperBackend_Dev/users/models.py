from django.db import models
from django.contrib.auth.models import User

#Modify this imports
from gis.models import *


# GCustomers - General customers 
class GCustomer(models.Model):
	gcustomer = models.AutoField(primary_key=True,db_index=True)
	user = models.ForeignKey(User)
	sector = models.ForeignKey()   # some pimary service area - based on this shows first recomendations
	phone = models.CharField(max_length=10,unique=True)
	registration_date = models.DateField(auto_now_add=True)
	registration_time = models.TimeField(auto_now_add=True)


# PCustomers - Professional Customers
class PCustomer(models.Model):
	pcustomer = models.AutoField(primary_key=True,db_index=True)
	user = models.ForeignKey(User)
	sector = models.ForeignKey()  # some pimary service area - based on this shows first recomendations
	phone = models.CharField(max_length=10,unique=True)
	registration_date = models.DateField(auto_now_add=True)
	registration_time = models.TimeField(auto_now_add=True)











