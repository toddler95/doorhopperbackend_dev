from django.db import models
from django.contrib.gis.db import models as gisModels
# Create your models here.

# Inter-City Area
class IDArea(gisModels.Model):
	idArea = models.AutoField(primary_key=True,db_index=True)
	idA_name = models.CharField(max_length=20)
	boundry = models.PolygonField(null=True)


# Sector
class Sector(gisModels.Model):
	sector = models.AutoField(primary_key=True,db_index=True)
	idArea = models.ForeignKey(IDArea)
	sector_name = models.CharField(max_length=20)
	boundry = models.PolygonField(null=True)









































































