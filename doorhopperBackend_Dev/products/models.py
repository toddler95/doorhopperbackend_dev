from django.db import models

# Create your models here.

SHOP_CATEGORY = (
	('AY','Anything'),
	('FD','Food'),
	('ST','Stationary'),
	('GY','Grocery'),
	('CS','Cosmetics'),
	('BK','Bakery'),
	('SW','Swadeshi'),
	('SW','Sweets'),
	('HW','Hardware'),
	('MS','Music'),
	('SP','Sports'),
	('EL','Electronics'),
	('ME','Medical'),
	)


# Products Details
class Products(models.Model):
	product = models.AutoField(primary_key=True,db_index=True)
	product_name = models.CharField(max_length=50)
	brand = models.CharField(max_length=20)
	barcode = models.CharField(max_length=128,db_index=True)
	shopCategory = models.CharField(max_length=,choices=SHOP_CATEGORY)

# Products Attributes
class ProductAttributes(models.Model):
	attribute = models.AutoField(primary_key=True)
	ofProduct = models.ForeignKey(Products,db_index=True)
	attribute_groupName = models.CharField(max_length=20)
	attribute_name = models.CharField(max_length=20)
	attribute_value = models.CharField(max_length=20)

# Products Category - Used for Catalogueing
class ProductsCategory(models.Model):
	category_id = models.AutoField(primary_key=True)
	category_name = models.CharField(max_length=20)
	parent_category = models.ForeignKey(ProductsCategory,null=True)
	category_level = models.PositiveSmallIntegerField()
	has_child = models.BinaryField(default=False)

# Tags
class PTags(models.Model):
	ptag_id = models.AutoField(primary_key=True)
	ptagWord = models.CharField(max_length=20)

# Tag relations
class PTagRelations(models.Model):
	ptag_id = models.ForeignKey(PTags)
	product_id = models.ForeignKey(Products)

	class Meta:
		index_together = [ "ptag_id","product_id" ]
