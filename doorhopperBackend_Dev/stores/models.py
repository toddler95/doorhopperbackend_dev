from django.db import models
from django.contrib.gis.db import models as gisModels
# Create your models here.

from products.models import Products
from services.models import Services
from merchants.models import Merchants
from gis.models import IDArea,Sector

QUANTITY_TYPE = (
	('WT','WEIGHT'),
	('VL','VOLUME'),
	('LN','LENGTH'),
	('AR','AREA'),
	('TP','TEMPERATURE'),
	('TM','TIME'),
	('QT','QUANTITY'),
	)

QUANTITY_UNIT = (
	('kg','Kilogram'),
	('gm','gram'),
	('mg','miliram'),
	('mr','metre'),
	('km','kilometer'),
	('ft','foot'),
	('cm','centimeter'),
	('ih','inch'),
	('li','litre'),
	('ml','mililitre'),
	('m2','metre_square'),
	('c2','centimetre_square'),
	('f2','suuare_foot'),
	('cl','celcius'),
	('dg','degree'),
	('fh','fahrenheit'),
	)

# Product Store Details

class ProductStore(gisModels.Model):
	productstore = gisModels.AutoField(primary_key=True,db_index=True)
	productstore_name = gisModels.CharField(max_length=20)
	owner = gisModels.ForeignKey(Merchants)
	idArea = gisModels.ForeignKey(IDArea)    	#   Foreignkey reference from gis module
	sector = gisModels.Foreignkey(Sector)		#	Foreignkey reference from gis module
	zipcode = models.CharField(max_length=6)
	location = gisModels.PointField()
	open_at = gisModels.TimeField()
	close_at = gisModels.TimeField()	

# Store Catalogue
class ProductStoreCatalogue(models.Model):
	storecatalogue = models.AutoField(primary_key=True,db_index=True)
	productstore = models.ForeignKey(ProductStore)
	category_name = models.CharField(max_length=20)
	parent_category = models.ForeignKey(ProductStoreCatalogue,null=True)
	category_level = models.PositiveSmallIntegerField()
	has_child = models.BinaryField(default=False)

# Product Store Content -  ( For InterDeliverable network, we have One model )
class ProductStoreContent(models.Model):
	content = models.AutoField(primary_key=True,db_index=True)
	store = models.ForeignKey(ProductStore)
	product = models.ForeignKey(Products)
	revenue = models.DecimalField(max_digits=4, decimal_places=2)
	availability = models.BinaryField(default=True)
	catalogue = models.ForeignKey(ProductStoreCatalogue,null=True)
	thumbnail = models.FileField(upload_to=None, max_length=100, attributes)	# Image Field
	ratings = models.DecimalField(max_digits=2, decimal_places=1,default=0.0)

# Product Variant
class ProductStoreContentVariant(models.Model):
	variant = models.AutoField(primary_key=True,db_index=True)
	parent_product = models.ForeignKey(ProductStoreContent)
	image = models.FileField(upload_to=, max_length=100, attributes)
	stock = models.SmallIntegerField() 
	min_stock = models.PositiveSmallIntegerField()  # minimum stock for notification	
	actual_price = models.DecimalField(max_digits=7, decimal_places=2)
	offer_price = models.DecimalField(max_digits=7, decimal_places=2)
	quantity_type = models.CharField(max_length=2,choices=QUANTITY_TYPE)
	quantity_unit = models.CharField(max_length=2,choices=QUANTITY_UNIT)
	quantity_val = models.DecimalField(max_digits=9, decimal_places=2)

# Service Store
class ServiceStore(gisModels.Model):
	servicestore = gisModels.AutoField(primary_key=True,db_index=True)
	servicestore_name = gisModels.CharField(max_length=20)
	owner = gisModels.ForeignKey(Merchants)
	idArea = gisModels.ForeignKey(IDArea)    	#   Foreignkey reference from gis module
	sector = gisModels.Foreignkey(Sector)		#	Foreignkey reference from gis module
	zipcode = models.CharField(max_length=6)
	location = gisModels.PointField()
	open_at = gisModels.TimeField()
	close_at = gisModels.TimeField()

# Service Catalogue
class ServiceStoreCatalogue(models.Model):
	storecatalogue = models.AutoField(primary_key=True,db_index=True)
	servicestore = models.ForeignKey(ServiceStore)
	category_name = models.CharField(max_length=20)
	parent_category = models.ForeignKey(ServiceStoreCatalogue,null=True)
	category_level = models.PositiveSmallIntegerField()
	has_child = models.BinaryField(default=False)

# Service Store Content - ( For InterDeliverable network, we have One model )
class ServiceStoreContent(models.Model):
	content = models.AutoField(primary_key=True,db_index=True)
	store = models.ForeignKey(ServiceStore)
	product = models.ForeignKey(Services)
	revenue = models.DecimalField(max_digits=4, decimal_places=2, attributes)
	availability = models.BinaryField(default=True)
	actual_rate = models.DecimalField(max_digits=7, decimal_places=2)
	offer_rate = models.DecimalField(max_digits=7, decimal_places=2)
	price_unit = models.CharField(max_length=10)
	catalogue = models.ForeignKey(ProductStoreCatalogue,null=True)
	thumbnail = models.FileField(upload_to=None, max_length=100, attributes)			# Image Field
	ratings = models.DecimalField(max_digits=2, decimal_places=1,default=0.0)

# Service Variant - 
class ServiceStoreContentVariant(models.Model):
	variant = models.AutoField(primary_key=True,db_index=True)
	parent_product = models.ForeignKey(ServiceStoreContent)
	actual_price = models.DecimalField(max_digits=7, decimal_places=2)
	offer_price = models.DecimalField(max_digits=7, decimal_places=2)
	quantity_type = models.CharField(max_length=2,choices=QUANTITY_TYPE)
	quantity_unit = models.CharField(max_length=2,choices=QUANTITY_UNIT)
	quantity_val = models.DecimalField(max_digits=9, decimal_places=2)

# Tax Details
class Tax(models.Model):
	tax = models.AutoField(primary_key=True,db_index=True)
	tax_percent = models.DecimalField(max_digits=5,decimal_places=4)
	tax_type = models.CharField(max_length=10)
	tax_name = models.CharField(max_length=10)
